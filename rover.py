"""
File: rover.py
Description: Entry point for running the robot from the keyboard
"""

from droids import RoverDroid

rover = RoverDroid()

rover.run()
