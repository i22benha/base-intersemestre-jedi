"""
File: orientation.py
Description: Provides structures and function for dealing with the orientation of the robot
"""

from enum import IntEnum, unique


@unique
class Orientation(IntEnum):
    '''
    Name of the directions used during the robot moves
    '''
    WEST = 1
    NORTH = 2
    EAST = 3
    SOUTH = 4


def inverse_direction(direction: Orientation):
    '''
    Given an Orientation, returns the Orientation after turning 180 deg

    :param Orientation direction: initial direction
    '''
    #TODO: Implement


def get_left(direction: Orientation):
    '''
    Given an Orientation, returns the Orientation after turning left

    :param Orientation direction: initial direction
    '''
    #TODO: Implement


def get_right(direction: Orientation):
    '''
    Given an Orientation, returns the Orientation after turning right

    :param Orientation direction: initial direction
    '''
    #TODO: Implement


class OrientationError(Exception):
    """
    Class for exception related to orientation
    """
