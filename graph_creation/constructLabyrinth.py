"""
File: constructLabyrinth.py
Description: Entry point for building the labyrinth explored by the robot
"""

import os
# import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from .orientation import Orientation
from ..controller import IntersectionType


class Labyrinth():
    """
    Represents a labyrinth
    """
    entry_name = "ENTRY"
    exit_name = "EXIT"

    def __init__(self, name):
        self.name = name
        # self.tree = nx.DiGraph()
        self.tree = nx.DiGraph()

    def add_intersection(self, type_intersection: IntersectionType,
                         parent_name: str, direction: Orientation):
        '''
        Adds an intersection to the graph

        :param IntersectionType type_intersection: what kind of intersection to add
        :param str parent_name: name of the nodes where to insert the intersection
        :param Orientation direction: the direction of the robot
        '''
        #TODO: Implement

    def add_new_node(self, parent_name, direction: Orientation):
        '''
        Adds a new node to the graph

        :param parent_name: name of the nodes where to insert the intersection
        :param Orientation direction: the direction of the robot
        '''
        #TODO Implement

    def add_entry(self, direction: Orientation):
        '''
        Adds the first node of the graph, corresponding to the entry of
        the labyrinth. This node is named ENTRY

        :param Orientation direction: the direction of the robot
        '''
        #TODO Implement

    def add_exit(self, tmp_name, direction: Orientation):
        '''
        Adds the node named EXIT to the graph, corresponding to the exit of
        the labyrinth.

        :param Orientation direction: the direction of the robot
        '''
        #TODO Implement


    def get_dir_child(self, parent_name, direction: Orientation):
        '''
        Returns the name of  a new node to the graph

        :param str parent_name: name of the parent nodes
        :param Orientation direction: the direction of child node whose name is asked
        '''
        #TODO Implement


    def go_to_next(self, parent_name: str, direction: Orientation):
        '''
        Returns the name of the child node we will reach from parent_name
        when going in the direction direction

        :param str parent_name: name of the parent nodes
        :param Orientation direction: the direction of child node where we want to go
        '''
        #TODO Implement


    def draw(self, withlabels=True):
        '''
        Draws the graph

        :param bool withlabels: if True the label of the edges is shown, else it
        is hidden
        '''
        #TODO Implement



def main():
    #TODO: Implement the graph creation



if __name__ == "__main__":
    main()
